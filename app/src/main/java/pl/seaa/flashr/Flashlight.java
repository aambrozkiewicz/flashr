package pl.seaa.flashr;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.Log;

import java.io.IOException;


public class Flashlight {
    private final String TAG = getClass().getName();

    private Camera camera;

    public void on() {
        camera = Camera.open();

        Camera.Parameters parameters = camera.getParameters();

        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);

        try {
            camera.setParameters(parameters);
            camera.setPreviewTexture(new SurfaceTexture(0));
            camera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public void off() {
        Camera.Parameters parameters = camera.getParameters();

        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

        camera.stopPreview();
        camera.release();
    }
}
