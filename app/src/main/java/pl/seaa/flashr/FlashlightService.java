package pl.seaa.flashr;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.provider.Telephony;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class FlashlightService extends Service {
    Flashlight flashlight = new Flashlight();

    @Override
    public void onCreate() {
        super.onCreate();

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(new PhoneStateListener() {
            private boolean ringing = false;

            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                SharedPreferences sharedPreferences = FlashlightService.this.getSharedPreferences(
                        MainActivity.SHARED_PREFS, MODE_PRIVATE);
                if (!sharedPreferences.getBoolean(getString(R.string.setting_call), true)) {
                    return;
                }

                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    flashlight.on();
                    ringing = true;
                } else if ((state == TelephonyManager.CALL_STATE_IDLE
                        || state == TelephonyManager.CALL_STATE_OFFHOOK)
                        && ringing) {

                    flashlight.off();
                    ringing = false;
                }
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);

        IntentFilter smsFilter = new IntentFilter();
        smsFilter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = FlashlightService.this.getSharedPreferences(
                        MainActivity.SHARED_PREFS, MODE_PRIVATE);
                if (!sharedPreferences.getBoolean(getString(R.string.setting_sms), true)) {
                    return;
                }

                flashlight.on();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1997L);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            flashlight.off();
                        }
                    }
                }).start();
            }
        }, smsFilter);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
