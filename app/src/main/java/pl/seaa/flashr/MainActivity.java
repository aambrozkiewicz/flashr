package pl.seaa.flashr;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;


public class MainActivity extends Activity {
    public static final String SHARED_PREFS = "pl.seaa.flashr";

    private Flashlight flashlight = new Flashlight();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        ImageButton flashlightBtn = (ImageButton) findViewById(R.id.flashlight_btn);
        flashlightBtn.setOnClickListener(new View.OnClickListener() {
            private boolean state = false;
            private final Object stateLock = new Object();

            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (stateLock) {
                            if (!state) {
                                flashlight.on();
                            } else {
                                flashlight.off();
                            }

                            state = !state;
                        }
                    }
                }).start();
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        int[] prefToggles = { R.id.toggle_call_setting, R.id.toggle_sms_setting };
        for (int toggle : prefToggles) {
            CompoundButton prefCheckbox = (CompoundButton) findViewById(toggle);
            prefCheckbox.setChecked(sharedPreferences.getBoolean((String) prefCheckbox.getTag(),
                    true));
        }

        Intent flashlightServiceIntent = new Intent(this, FlashlightService.class);
        startService(flashlightServiceIntent);
    }

    @OnCheckedChanged({ R.id.toggle_call_setting, R.id.toggle_sms_setting })
    public void onPreferenceToggle(CompoundButton button, boolean isChecked) {
        String prefKey = (String) button.getTag();

        SharedPreferences sharedPreferences = MainActivity.this.getSharedPreferences(
                SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(prefKey, isChecked);

        editor.commit();
    }
}
